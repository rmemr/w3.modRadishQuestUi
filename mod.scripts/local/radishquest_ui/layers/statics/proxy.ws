// ----------------------------------------------------------------------------
class CRadishStaticsProxy extends CRadishPermanentProxy {
    // ------------------------------------------------------------------------
    private var meshPath: String;
    // ------------------------------------------------------------------------
    default templatePath = "dlc\modtemplates\radishquestui\flags\static.w2ent";
    default meshPath = "";
    // ------------------------------------------------------------------------
    public function setMeshPreviewTemplate(previewMeshTemplatePath: String, meshPath: String)
    {
        if (permanentTemplatePath != previewMeshTemplatePath) {
            permanentTemplatePath = previewMeshTemplatePath;
            this.meshPath = meshPath;
            despawnPermanent();
            spawnPermanent();
        }
    }
    // ------------------------------------------------------------------------
    public function getMeshPath() : String {
        return meshPath;
    }
    // ------------------------------------------------------------------------
    public function setSettings(newSettings: SRadishLayerStaticEntityData) {
        // mesh settings takes precedence
        if (newSettings.mesh != "") {
            if (newSettings.mesh != meshPath) {
                meshPath = newSettings.mesh;
                permanentTemplatePath = newSettings.template;
                despawnPermanent();
                spawnPermanent();
            }
        } else {
            if (newSettings.template != permanentTemplatePath) {
                permanentTemplatePath = newSettings.template;
                meshPath = "";
                despawnPermanent();
                spawnPermanent();
            }
        }

        if (placement != newSettings.placement) {
            placement = newSettings.placement;
            moveTo(placement);
        }
        if (permanentEntityScale != newSettings.scale) {
            permanentEntityScale = newSettings.scale;
            scaleTo(permanentEntityScale);
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

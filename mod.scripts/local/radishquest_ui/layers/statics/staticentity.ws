// ----------------------------------------------------------------------------
class CRadishQuestLayerStaticEntity extends CRadishLayerEntity {
    // ------------------------------------------------------------------------
    default entityType = "static";
    // ------------------------------------------------------------------------
    protected var settings: SRadishLayerStaticEntityData;
    // ------------------------------------------------------------------------
    protected var staticsProxy: CRadishStaticsProxy;
    // ------------------------------------------------------------------------
    public function init(layerId: SRadUiLayerId, newPlacement: SRadishPlacement)
    {
        // a valid default path
        settings.template = "environment\decorations\decoration_sets\boxes\decoration_set_boxes_chest_b.w2ent";
        settings.mesh = "";
        settings.placement = newPlacement;
        settings.scale = Vector(1.0, 1.0, 1.0, 1.0);
        super.init(layerId, newPlacement);
    }
    // ------------------------------------------------------------------------
    protected function initProxy() {
        // statics proxy template will be visibile from the start
        staticsProxy = new CRadishStaticsProxy in this;
        proxy = staticsProxy;
        proxy.init(settings.template, placement, this.initializedFromEntity);

        staticsProxy.setSettings(settings);
    }
    // ------------------------------------------------------------------------
    public function initFromData(data: SRadishLayerStaticEntityData) {
        initProxy();
        setSettings(data);
    }
    // ------------------------------------------------------------------------
    public function cloneFrom(src: CRadishLayerEntity) {
        var newSettings: SRadishLayerStaticEntityData;

        super.cloneFrom(src);

        newSettings = ((CRadishQuestLayerStaticEntity)src).getSettings();
        // placement is valid in all "other" types
        newSettings.placement = src.getPlacement();

        if (newSettings.template == "" && newSettings.mesh == "") {
            newSettings.template = "environment\decorations\decoration_sets\boxes\decoration_set_boxes_chest_b.w2ent";
        }
        this.initFromData(newSettings);
    }
    // ------------------------------------------------------------------------
    public function getDerivedName() : String {
        var derived: String;
        if (StrLen(settings.mesh) > 2) {
            derived = StrReplaceAll(settings.mesh, ".w2mesh", "");
        } else if (StrLen(settings.template) > 2) {
            derived = StrReplaceAll(settings.template, ".w2ent", "");
        }
        derived = StrReplaceAll(derived, StrChar(92), "/");
        derived = StrAfterLast(derived, "/");

        return derived;
    }
    // ------------------------------------------------------------------------
    // settings
    // ------------------------------------------------------------------------
    public function getSettings() : SRadishLayerStaticEntityData {
        settings.id = id.entityName;
        settings.entityname = id.entityName;

        // this has to be updated as base class contains the current placement data
        settings.placement = placement;
        return settings;
    }
    // ------------------------------------------------------------------------
    public function setSettings(newSettings: SRadishLayerStaticEntityData) {
        settings = newSettings;
        placement = settings.placement;
        setName(settings.entityname);

        staticsProxy.setSettings(settings);
    }
    // ------------------------------------------------------------------------
    public function setPlacement(newPlacement: SRadishPlacement) {
        super.setPlacement(newPlacement);
        // update local settings
        settings.placement = newPlacement;
    }
    // ------------------------------------------------------------------------
    protected function updateAppearanceSetting(id: name) {
        settings.appearance = id;
    }
    // ------------------------------------------------------------------------
    public function refreshRepresentation() {
        staticsProxy.setSettings(settings);
    }
    // ------------------------------------------------------------------------
    // settings editing (text input)
    // ------------------------------------------------------------------------
    public function addUiSettings(settingsList: CRadishUiSettingsList) {
        if (settings.mesh != "") {
            settingsList.addSetting("mesh", "mesh: " + UiFormatString(settings.mesh));
        } else {
            settingsList.addSetting("tags", "tags: [auto]");
            settingsList.addSetting("class", "class: " + settings.entityClass);
            settingsList.addSetting("template", "template: " + UiFormatString(settings.template));
        }
        settingsList.addSetting("pos", "position: " + UiSettingVecToString(settings.placement.pos));
        settingsList.addSetting("rot", "rotation: " + UiSettingAnglesToString(settings.placement.rot));
        settingsList.addSetting("scale", "scale: " + UiSettingVecToString(settings.scale));
    }
    // ------------------------------------------------------------------------
    public function getAsUiSetting(id: String) : IModUiSetting {
        var null: IModUiSetting;
        switch (id) {
            case "tags":    return ReadOnlyUiSetting(this);
            case "class":   return ReadOnlyUiSetting(this);
            case "template":return TemplateToUiSetting(this, settings.template);
            case "mesh":    return MeshToUiSetting(this, settings.mesh);
            case "pos":     return VecToUiSetting(this, settings.placement.pos);
            case "rot":     return AnglesToUiSetting(this, settings.placement.rot);
            case "scale":   return VecToUiSetting(this, settings.scale);
            default:        return null;
        }
    }
    // ------------------------------------------------------------------------
    public function syncSetting(id: String, settingValue: IModUiSetting) {
        switch (id) {
            case "pos":     placement.pos = UiSettingToVector(settingValue); break;
            case "rot":     placement.rot = UiSettingToAngles(settingValue); break;
            case "scale":   settings.scale = UiSettingToVector(settingValue); break;
            case "template":
                settings.mesh = "";
                settings.template = staticsProxy.getPermanentTemplate();
                settings.entityClass = staticsProxy.getPermanentClass();
                break;
            case "mesh":
                settings.mesh = staticsProxy.getMeshPath();
                settings.template = staticsProxy.getPermanentTemplate();
                settings.entityClass = "";
                break;
            default:    super.syncSetting(id, settingValue);
        }
        settings.placement = placement;
    }
    // ------------------------------------------------------------------------
    // definition creation
    // ------------------------------------------------------------------------
    public function asDefinition() : SEncValue {
        var content, dbgInfos, components, meshComponent, transform, flags: SEncValue;
        var stdScale: Vector;

        stdScale = Vector(1.0, 1.0, 1.0, 1.0);

        content = encValueNewMap();

        if (StrLen(settings.mesh) > 0) {
            // dbg infos (actual mesh & mesh preview)
            dbgInfos = encValueNewMap();
            encMapPush_str("mesh", settings.mesh, dbgInfos);
            encMapPush_str("meshpreview", settings.template, dbgInfos);

            // create a generic static entity with static mesh component
            //   .type: CEntity:
            //   transform:
            //      pos: [..]
            //      rot: [..]
            //   streamingDistance: 200     ?
            //   components:
            //     mesh:
            //       .type: CStaticMeshComponent
            //       isStreamed: true       ?
            //       drawableFlags:
            //         - IsVisible
            //         - CastShadows
            //       mesh: <..>.w2mesh

            components = encValueNewMap();
            meshComponent = encValueNewMap();
            transform = encValueNewMap();
            flags = encValueNewList();
            encListPush_str("IsVisible", flags);
            encListPush_str("CastShadows", flags);

            encMapPush_str(".type", "CStaticMeshComponent", meshComponent);
            encMapPush_bool("isStreamed", true, meshComponent);
            encMapPush("drawableFlags", flags, meshComponent);
            encMapPush_str("mesh", settings.mesh, meshComponent);
            encMapPush("mesh", meshComponent, components);

            encMapPush("pos", PosToEncValue(placement.pos), transform);
            encMapPush("rot", RotToEncValue(placement.rot), transform);
            if (settings.scale != stdScale) {
                encMapPush("scale", PosToEncValue(settings.scale), transform);
            }

            encMapPush_str(".type", "CEntity", content);
            encMapPush(".debug", dbgInfos, content);
            encMapPush("transform", transform, content);
            encMapPush_int("streamingDistance", 200, content);
            encMapPush("components", components, content);

        } else {
            if (StrLen(settings.entityClass) > 0) {
                encMapPush_str("template", settings.entityClass + ":" + settings.template, content);
            } else {
                encMapPush_str("template", settings.template, content);
            }
            encMapPush("pos", PosToEncValue(placement.pos), content);
            encMapPush("rot", RotToEncValue(placement.rot), content);
            if (settings.scale != stdScale) {
                encMapPush("scale", PosToEncValue(settings.scale), content);
            }
        }

        return content;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CRadishQuestLayerEncodedStaticEntity extends CRadishQuestLayerStaticEntity {
    // ------------------------------------------------------------------------
    protected var meshSize: Vector;
    // ------------------------------------------------------------------------
    public function initFromDbgInfos(entity: CEntity) {
        var boundingBox: Box;

        // extract scaling
        settings.scale = entity.GetLocalScale();

        super.initFromDbgInfos(entity);

        // extract mesh size from the *encoded* entity
        // try boundingbox first
        entity.CalcBoundingBox(boundingBox);
        meshSize = RadUi_extractMeshBoxSize(boundingBox, 0.25);

        if (meshSize.X == 0.25 && meshSize.Y == 0.25 && meshSize.Z == 0.25) {
            // entity doesn't have a boundingbox (yet?) return default box
            meshSize = Vector(0.5, 0.5, 0.5, 1.0);
        }
    }
    // ------------------------------------------------------------------------
    public function getSize() : Vector {
        return this.meshSize;
    }
    // ------------------------------------------------------------------------
    protected function restoreFromDbgInfo(dbgInfo: SDbgInfo) {
        switch (dbgInfo.type) {
            case "mesh":        settings.mesh = dbgInfo.s;  break;
            case "meshpreview": settings.template = dbgInfo.s;  break;
            case "template":    settings.template = dbgInfo.s;  break;
            case "class":       settings.entityClass = dbgInfo.s;  break;
        }
        settings.placement = placement;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CRadishQuestLayerEncodedStaticShadowEntity extends CRadishQuestLayerEncodedStaticEntity {
    default specialization = "shadows";
}
// ----------------------------------------------------------------------------

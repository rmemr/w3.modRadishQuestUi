// ----------------------------------------------------------------------------
class CRadishQuestLayerWaterArea extends CRadishQuestLayerArea {
    // ------------------------------------------------------------------------
    default specialization = "water";
    // ------------------------------------------------------------------------
    public function initFromDbgInfos(entity: CEntity) {
        super.initFromDbgInfos(entity);

        // default color
        setAppearance('blue');
    }
	public function cloneFrom(src: CRadishLayerEntity) {
        super.cloneFrom(src);

        // default color
        setAppearance('blue');
    }
    // ------------------------------------------------------------------------
    // definition creation
    // ------------------------------------------------------------------------
    public function asDefinition() : SEncValue {
        var entity, transform, components, waterarea, bpList: SEncValue;
        var i: int;

        // generic static entity with envarea component:
        //  .type: CGameplayEntity
        //  transform:
        //    pos: [-110.0, -200.0, 30.0]
        //    scale: [ 1.0, 1.0, 1.0]
        //  components:
        //    envcomponent:
        //      .type: CWaterComponent
        //      ...

        entity = encValueNewMap();
        transform = encValueNewMap();
        components = encValueNewMap();

        waterarea = encValueNewMap();

        // -- waterarea
        encMapPush_str(".type", "CWaterComponent", waterarea);

        bpList = encValueNewList();
        for (i = 0; i < settings.border.Size(); i += 1) {
            encListPush(Pos4ToEncValue(settings.border[i] - placement.pos, true), bpList);
        }
        encMapPush("localPoints", bpList, waterarea);

        // -- transform
        encMapPush("pos", PosToEncValue(this.placement.pos), transform);
        encMapPush("scale", PosToEncValue(Vector(1.0, 1.0, 1.0)), transform);

        // -- components
        encMapPush("water", waterarea, components);

        // -- entity
        encMapPush_str(".type", "CGameplayEntity", entity);
        encMapPush("transform", transform, entity);
        encMapPush("components", components, entity);

        return entity;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
class CModUiEntityMeshUiSetting extends CModUiGenericListUiSetting {
    // ------------------------------------------------------------------------
    default valueListId = "entity_meshes";
    default workModeState = 'RadUi_StaticsMeshSelection';
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
function MeshToUiSetting(parentObj: CObject, value: String) : CModUiEntityMeshUiSetting
{
    var s: CModUiEntityMeshUiSetting;
    s = new CModUiEntityMeshUiSetting in parentObj;
    s.value = value;

    return s;
}
// ----------------------------------------------------------------------------

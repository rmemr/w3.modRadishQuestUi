// ----------------------------------------------------------------------------
abstract state RadUi_BaseInteractiveCamera in CRadishListViewWorkMode extends Rad_InteractiveCamera
{
    default workContext = 'MOD_RadishUi_ModeInteractiveCam';
    // ------------------------------------------------------------------------
    protected function backToPreviousState(action: SInputAction) {
        parent.backToPreviousState(action);
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        var staticCam: CRadishStaticCamera;

        // interactive cam MUST be stopped before changing to static cam!
        theCam.stopInteractiveMode();

        // reactivate the static cam *AFTER* destroying the interactive one
        staticCam = getManagerCam();
        staticCam.setSettings(theCam.getActiveSettings());
        staticCam.switchTo();

        super.OnLeaveState(nextStateName);
        parent.notice(GetLocStringByKeyExt("RAD_iCamInteractiveStop"));
    }
    // ------------------------------------------------------------------------
    event OnChangeWorkMode(action: SInputAction) {
        // direct jump to top level required
        parent.backToParent(action);
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        super.registerListeners();
        theInput.RegisterListener(this, 'OnChangeWorkMode', 'RADUI_BackToTop');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        super.unregisterListeners();
        theInput.UnregisterListener(this, 'RADUI_BackToTop');
    }
    // ------------------------------------------------------------------------
    protected function getManagerCam() : CRadishStaticCamera;
    // ------------------------------------------------------------------------
    protected function createCam() : CRadishInteractiveCamera {
        var staticCam: CRadishStaticCamera;
        var cam: CRadishInteractiveCamera;

        staticCam = getManagerCam();

        cam = createAndSetupInteractiveCam(
            parent.config, staticCam.getActiveSettings(), staticCam.getTracker());

        // ahead of view tracking causes visibility glitches in interiors because
        // player actor may be positioned outside walls of current room
        // -> occlusion trigger glitches of inside meshes
        // Note:
        // thePlayer.IsInInterior cannot be used because player is hidden and
        // TC_DEFAULT triggerchannel deactivated to prevent accidental area
        // triggers while camera is moving and player is teleported around
        // -> use ahead of tracking state as initialized upon radui ui maximization
        cam.enableAheadOfViewTrackingPosition(staticCam.isAheadOfViewTrackingActive());

        return cam;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadUi_InteractiveCamera in CRadishQuestLayerEntityMode extends RadUi_BaseInteractiveCamera
{
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        if (parent.config.isAutoCamOnSelect()) {
            parent.config.setAutoCamOnSelect(false);
            parent.notice(
                GetLocStringByKeyExt("RAD_iCamInteractive") + " " +
                GetLocStringByKeyExt("RADUI_iCamFollowOff")
            );
        } else {
            parent.notice(GetLocStringByKeyExt("RAD_iCamInteractive"));
        }
        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    protected function getManagerCam() : CRadishStaticCamera {
        return parent.itemManager.getCam();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadUi_InteractiveCamera in CRadishQuestLayerMode extends RadUi_BaseInteractiveCamera
{
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        if (parent.config.isAutoCamOnSelect()) {
            parent.config.setAutoCamOnSelect(false);
            parent.notice(
                GetLocStringByKeyExt("RAD_iCamInteractive") + " " +
                GetLocStringByKeyExt("RADUI_iCamFollowOff")
            );
        } else {
            parent.notice(GetLocStringByKeyExt("RAD_iCamInteractive"));
        }
        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    protected function getManagerCam() : CRadishStaticCamera {
        return parent.layerManager.getCam();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadUi_InteractiveCamera in CRadishQuestLayerSearchMode extends RadUi_BaseInteractiveCamera
{
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        if (parent.config.isAutoCamOnSelect()) {
            parent.config.setAutoCamOnSelect(false);
            parent.notice(
                GetLocStringByKeyExt("RAD_iCamInteractive") + " " +
                GetLocStringByKeyExt("RADUI_iCamFollowOff")
            );
        } else {
            parent.notice(GetLocStringByKeyExt("RAD_iCamInteractive"));
        }
        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    protected function getManagerCam() : CRadishStaticCamera {
        return parent.layerManager.getCam();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadUi_InteractiveCamera in CRadishCommunityMode extends RadUi_BaseInteractiveCamera
{
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        if (parent.config.isAutoCamOnSelect()) {
            parent.config.setAutoCamOnSelect(false);
            parent.notice(
                GetLocStringByKeyExt("RAD_iCamInteractive") + " " +
                GetLocStringByKeyExt("RADUI_iCamFollowOff")
            );
        } else {
            parent.notice(GetLocStringByKeyExt("RAD_iCamInteractive"));
        }

        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    protected function getManagerCam() : CRadishStaticCamera {
        return parent.communityManager.getCam();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadUi_InteractiveCamera in CRadishCommunityElementMode extends RadUi_BaseInteractiveCamera
{
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        if (parent.config.isAutoCamOnSelect()) {
            parent.config.setAutoCamOnSelect(false);
            parent.notice(
                GetLocStringByKeyExt("RAD_iCamInteractive") + " " +
                GetLocStringByKeyExt("RADUI_iCamFollowOff")
            );
        } else {
            parent.notice(GetLocStringByKeyExt("RAD_iCamInteractive"));
        }
        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    protected function getManagerCam() : CRadishStaticCamera {
        return parent.itemEditor.getCam();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

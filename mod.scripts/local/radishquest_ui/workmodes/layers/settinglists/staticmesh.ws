// ----------------------------------------------------------------------------
state RadUi_StaticsMeshSelection in CRadishQuestLayerEntityMode
    extends RadUi_GenericListSettingSelection
{
    // ------------------------------------------------------------------------
    default labelPrefixId = "StaticMesh";
    default changedInfoId = "RADUI_iChangedEntityMesh";
    default selectPrefixId = "EntityMesh";
    // ------------------------------------------------------------------------
    protected function onListSelection(selectedId: String) {
        var proxy: CRadishStaticsProxy;
        var setting: CModUiEntityMeshUiSetting;
        var previewId, previewPath: String;

        super.onListSelection(selectedId);

        setting = (CModUiEntityMeshUiSetting)editedSetting;
        proxy = (CRadishStaticsProxy)selected.getProxy();

        // get preview template
        previewId = ((CGenericListSettingList)listProvider).getExtraData(
            setting.getValueId(), 6);

        // radui mesh preview paths contain only the filename because the preview
        // entities are stored in the same path. but custom meshes may have
        // different paths
        if (StrFindFirst(previewId, "/") >= 0 || StrFindFirst(previewId, StrChar(92)) >= 0) {
            previewPath = previewId;
        } else {
            previewPath = "dlc/modtemplates/radishquestui/meshes/" + previewId;
        }

        proxy.setMeshPreviewTemplate(previewPath, setting.getValueId());
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

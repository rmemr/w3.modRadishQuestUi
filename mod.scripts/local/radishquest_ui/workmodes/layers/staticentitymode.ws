// ----------------------------------------------------------------------------
state RadUi_InteractivePlacement in CRadishQuestLayerStaticEntityMode
    extends RadUi_BaseEntityInteractivePlacement
{
    default isRotatableYaw = true;
    default isRotatablePitch = true;
    default isRotatableRoll = true;
}
// ----------------------------------------------------------------------------
state RadUi_StaticEntityEditing in CRadishQuestLayerStaticEntityMode extends RadUi_EntityEditing
{
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        entityEditor = new CRadishQuestLayerStaticEntityEditor in this;
        entity = parent.itemManager.getSelected();

        super.setupLabels("StaticEntity");
        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        // only default static entities use the template hotkey
        if (unlocked && entity.getSpecialization() == "") {
            hotkeyList.PushBack(HotkeyHelp_from('RADUI_SelectTemplate', "RADUI_SelectStaticsTemplate"));
            hotkeyList.PushBack(HotkeyHelp_from('RADUI_SelectMesh', "RADUI_SelectStaticsMesh"));
        }
        super.OnEntityHotkeyHelp("StaticEntity", hotkeyList);
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        super.registerListeners();
        // only default static entities use the template hotkey
        if (entity.getSpecialization() == "") {
            if (unlocked) {
                theInput.RegisterListener(parent, 'OnSelectTemplate', 'RADUI_SelectTemplate');
                theInput.RegisterListener(parent, 'OnSelectMesh', 'RADUI_SelectMesh');
            } else {
                theInput.RegisterListener(parent, 'OnLockedMode', 'RADUI_SelectTemplate');
                theInput.RegisterListener(parent, 'OnLockedMode', 'RADUI_SelectMesh');
            }
        }
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        super.unregisterListeners();
        theInput.UnregisterListener(parent, 'RADUI_SelectTemplate');
        theInput.UnregisterListener(parent, 'RADUI_SelectMesh');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadUi_StaticEntityManaging in CRadishQuestLayerStaticEntityMode extends RadUi_EntityManaging
{
    default editStateName = 'RadUi_StaticEntityEditing';
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        super.setupLabels("StaticEntity");
        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    event OnAutoRename(action: SInputAction) {
        var derivedName, newName: String;
        var i: int;

        if (selectedEntity.getSpecialization() == "") {
            if (unlocked && IsPressed(action)
                && itemManager.getSelected()
                && !parent.view.listMenuRef.isEditActive()) {

                derivedName = ((CRadishQuestLayerStaticEntity)selectedEntity).getDerivedName();

                if (StrLen(derivedName) > 2) {

                    newName = derivedName;
                    while (!itemManager.verifyName(newName)) {
                        i += 1;
                        newName = derivedName + " " + IntToString(i);
                    }
                    itemManager.renameSelected(newName);
                    listProvider.setSelection(selectedEntity.getIdString(), true);
                    parent.log.debug("auto renamed entity: " + newName);

                    this.updateView();
                } else {
                    parent.error(GetLocStringByKeyExt("RADUI_eAutoRenameFailed"));
                }
            }
        }
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        if (unlocked) {
            hotkeyList.PushBack(HotkeyHelp_from('RADUI_SelectTemplate', "RADUI_SelectStaticsTemplate"));
            hotkeyList.PushBack(HotkeyHelp_from('RADUI_SelectMesh', "RADUI_SelectStaticsMesh"));
            hotkeyList.PushBack(HotkeyHelp_from('RADUI_AutoRename', "RADUI_AutorenameEntity"));
        }
        super.OnEntityHotkeyHelp("StaticEntity", hotkeyList);
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        super.registerListeners();
        if (unlocked) {
            theInput.RegisterListener(parent, 'OnSelectTemplate', 'RADUI_SelectTemplate');
            theInput.RegisterListener(parent, 'OnSelectMesh', 'RADUI_SelectMesh');
            theInput.RegisterListener(parent, 'OnAutoRename', 'RADUI_AutoRename');
        } else {
            theInput.RegisterListener(parent, 'OnLockedMode', 'RADUI_SelectTemplate');
            theInput.RegisterListener(parent, 'OnLockedMode', 'RADUI_SelectMesh');
            theInput.RegisterListener(parent, 'OnLockedMode', 'RADUI_AutoRename');
        }
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        super.unregisterListeners();
        theInput.UnregisterListener(parent, 'RADUI_SelectTemplate');
        theInput.UnregisterListener(parent, 'RADUI_SelectMesh');
        theInput.UnregisterListener(parent, 'RADUI_AutoRename');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Top level management of StaticEntities
//
statemachine class CRadishQuestLayerStaticEntityMode extends CRadishQuestLayerEntityMode {
    default workMode = 'RADUI_QL_ModeStaticEntities';
    default workContext = 'MOD_RadishUi_QL_ModeStaticEntities';
    default generalHelpKey = "RADUI_StaticEntityGeneralHelp";
    default defaultState = 'RadUi_StaticEntityManaging';
    // ------------------------------------------------------------------------
    event OnSelectTemplate(action: SInputAction) {
        // only default static entities use the template hotkey
        if (selectedEntity.getSpecialization() == "" || selectedEntity.getSpecialization() == "shadows") {
            if (unlocked && IsPressed(action) && itemManager.getSelected() && !view.listMenuRef.isEditActive()) {
                entityEditor = new CRadishQuestLayerStaticEntityEditor in this;
                itemManager.getSelected().highlight(true);
                entityEditor.init(log, itemManager.getSelected());
                entityEditor.select("template");
                PushState('RadUi_StaticsTemplateSelection');
            }
        }
    }
    // ------------------------------------------------------------------------
    event OnSelectMesh(action: SInputAction) {
        // only default static entities use the mesh hotkey
        if (selectedEntity.getSpecialization() == "" || selectedEntity.getSpecialization() == "shadows") {
            if (unlocked && IsPressed(action) && itemManager.getSelected() && !view.listMenuRef.isEditActive()) {
                entityEditor = new CRadishQuestLayerStaticEntityEditor in this;
                itemManager.getSelected().highlight(true);
                entityEditor.init(log, itemManager.getSelected());
                entityEditor.select("mesh");
                PushState('RadUi_StaticsMeshSelection');
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

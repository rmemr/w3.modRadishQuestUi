exec function rad_set_fact(factId: string, optional value: int) {
    if (value != 0) {
        FactsAdd( factId, value, -1);
    } else {
        FactsAdd( factId, 1, -1);
    }
}

exec function rad_del_fact(factId: string) {
    if (FactsDoesExist( factId )) {
        FactsRemove(factId);
        theGame.GetGuiManager().ShowNotification("fact removed: " + factId);
    }
}

exec function rad_fact(factId: string) {
    theGame.GetGuiManager().ShowNotification("fact [" + factId + "]: " + FactsQuerySum(factId));
}

exec function rad_scene(scenepath: string, optional inputSocket: String) {
    var scene : CStoryScene;
    var emptyString: String;

    if (inputSocket == emptyString) {
        inputSocket = "Input";
    }

    scene = (CStoryScene)LoadResource(scenepath + ".w2scene", true);
    theGame.GetStorySceneSystem().PlayScene(scene, inputSocket);
}

function rad_spawn_t(tmpl: String) {
    var pos: Vector;
    var template: CEntityTemplate;
    var entity: CEntity;
    var templatePath: String;
    var rot: EulerAngles;

    pos = thePlayer.GetWorldPosition() + Vector(1.0, 1.0, -0.4);
    templatePath = tmpl;

    template = (CEntityTemplate)LoadResource(templatePath, true);
    entity = theGame.CreateEntity(template, pos, rot);
    if (entity) {
        theGame.GetGuiManager().ShowNotification("success " + tmpl);
    } else {
        theGame.GetGuiManager().ShowNotification("entity creation failed");
    }
}

function rad_spawn_flag(pos: Vector, optional rotation: float, optional type: int) {
    var template: CEntityTemplate;
    var entity: CEntity;
    var templatePath: String;
    var rot: EulerAngles;

    switch (type) {
        case 1: templatePath = "engine\templates\editor\markers\review\opened_flag.w2ent"; break;
        case 2: templatePath = "engine\templates\editor\markers\review\closed_flag.w2ent"; break;
        default: templatePath = "engine\templates\editor\markers\review\fixed_flag.w2ent"; break;
    }

    rot.Yaw = rotation;
    template = (CEntityTemplate)LoadResource(templatePath, true);
    entity = theGame.CreateEntity(template, pos, rot);
}

exec function rad_spawn(tmpl: String) {
    rad_spawn_t(tmpl);
}

exec function rad_tele(optional x: float, optional y: float, optional z: float) {
    var pos: Vector;
    var rot: EulerAngles;

    if (theGame.IsFreeCameraEnabled()) {
        pos = theGame.GetFreeCameraPosition();
    } else {
        pos = theCamera.GetCameraPosition();
    }
    rot.Yaw = theCamera.GetCameraHeading();

    if (x != 0.0) { pos.X = x; }
    if (y != 0.0) { pos.Y = y; }
    if (z != 0.0) { pos.Z = z; }

    thePlayer.TeleportWithRotation(pos, rot);
}

exec function rad_pos() {
    var pos: Vector;

    pos = thePlayer.GetWorldPosition();
    theGame.GetGuiManager().ShowNotification(FloatToString(pos.X) + " " + FloatToString(pos.Y) + " " + FloatToString(pos.Z));
}

exec function rad_goto(hubname: String, optional waypoint: CName) {
    var destWorldPath   : string;
    var areaId: EAreaName;
    var wp: CName;

    switch(hubname) {
        case "novigrad":
        case "novi":
            areaId = AN_NMLandNovigrad;
            wp = '';
            break;

        case "skellige":
            areaId = AN_Skellige_ArdSkellig;
            wp = '';
            break;

        case "kaer_morhen":
        case "km":
            areaId = AN_Kaer_Morhen;
            wp = '';
            break;

        case "prolog_village":
        case "prolog":
        case "white_orchard":
            areaId = AN_Prologue_Village;
            wp = '';
            break;

        case "wyzima_castle":
        case "wyzima":
        case "vyzima":
        case "vizima":
            areaId = AN_Wyzima;
            wp = '';
            break;

        case "island_of_mist":
        case "mist":
            areaId = AN_Island_of_Myst;
            wp = '';
            break;

        case "spiral":
            areaId = AN_Spiral;
            wp = '';
            break;

        case "no_mans_land":
        case "nml":
            areaId = AN_Velen;
            wp = '';
            break;

        case "bob":
            areaId = AN_Bob;
            wp = '';
            break;

        default: {
            areaId = AreaNameToType(hubname);
        }
    }

    if (waypoint != '') {
        wp = waypoint;
    }

    destWorldPath = theGame.GetCommonMapManager().GetWorldPathFromAreaType(areaId);
    theGame.ScheduleWorldChangeToMapPin(destWorldPath, wp);
}
